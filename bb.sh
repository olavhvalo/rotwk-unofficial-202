#!/usr/bin/env bash

git diff -z --name-only master HEAD "$PWD/source/" ':(exclude)source/lang' | xargs --null cp --parents -t ~/Documents
cd ~/Documents/source && bigtool -c ../\!beta.big * 
mv ~/Documents/\!beta.big "$1"
cd $PWD
rm -r ~/Documents/source
