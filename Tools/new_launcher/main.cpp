/*
 * main.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

// For GUI
#include <QApplication>
#include <QWidget>
#include <QDebug>

#include "Release.h"
#include "SwitchFile.h"
#include "ConfigParser.h"
#include "LauncherWindow.h"

#include <QSettings>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
	qDebug() << "Number of args:" << argc;

	qApp->setApplicationName("2.02 patch launcher");
	qApp->setOrganizationName("RotWK 2.02");

	QSettings::setDefaultFormat(QSettings::IniFormat);

	QApplication app(argc, argv);
	//~ app.setAttribute(Qt::AA_DontShowIconsInMenus, true);

	LauncherWindow window(argc);
	window.resize(1, 1); //set to smallest possible size
	window.setMaximumWidth(1); window.setMaximumHeight(1); //lock to that size

	window.setWindowTitle("RotWK 2.02 launcher");

	window.setGeometry(
		QStyle::alignedRect(
			Qt::LeftToRight,
			Qt::AlignCenter,
			QSize(285, 400),  // this should be an approximation of how large the launcher actually is
			qApp->desktop()->availableGeometry()
		)
	); 

	if(argc == 1)
		window.show();

	return app.exec();
}
