/*
 * LauncherWindow.h
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#pragma once

#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QAction>
#include <QLabel>

// For console output
#include <QTextStream>

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>

// For md5sum
#include <QCryptographicHash>

#include <QComboBox>
#include <QMainWindow>

#include "ConfigParser.h"
#include "Release.h"
#include "ResTool.h"
#include "ReplayTool.h"

class LauncherWindow : public QMainWindow
{
	Q_OBJECT

	public:
		LauncherWindow(int argc);

	private slots:
		void ApplyRelease();
		void VerifyRelease();
		void OpenAboutPage();
		void OpenTechSupportPage();
		void LaunchReptool();
		void LaunchResfix();

	private:
		QComboBox *cbox;
		QComboBox *cbox2;
		QComboBox *cbox3;
		QComboBox *cbox4;
		QPushButton *applyBtn;
		QPushButton *apply2Btn;
		QPushButton *apply3Btn;
		QPushButton *apply4Btn;
		ResTool *ResToolPopup;
		ReplayTool *RepToolPopup;
};
