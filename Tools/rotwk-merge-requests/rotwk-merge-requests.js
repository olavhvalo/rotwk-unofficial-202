const axios = require("axios").default;
const fs = require("fs");

async function fetchOpenedMRQpage(page, allbranches) {
  try {
    const response = await axios.get(
      "https://gitlab.com/api/v4/projects/247755/merge_requests?scope=all&state=opened&page=".concat(
        page
      )
    );

    response.data.forEach(item => {
      if (!item.work_in_progress) {
        let branchName = "origin/".concat(item.source_branch);
        console.log(item.title);
        console.log("Labels:", item.labels.join(", "));
        console.log("Created: ", item.created_at);
        console.log(branchName, "\n");
        allbranches.push({
          requestName: item.title,
          branchName,
          author: item.author.name
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
  return allbranches;
}

async function main(param) {
  let promises = [];

  for (i = 1; i <= param.maxPages; i++) {
    promises.push(fetchOpenedMRQpage(i, []));
  }
  let results = await Promise.all(promises);

  console.log("----------------------------------------------------");

  const script1 = fs.createWriteStream("./mergescript.sh");
  const script2 = fs.createWriteStream("./mergescriptsquash.sh");

  [].concat.apply([], results).forEach(item => {
    const requestName = item.requestName.replace("!", "\\!");
    script2.write(`if [[ $(git log | grep -c "${requestName}") = 0 ]]; then\n`);
    script2.write(`    echo "${item.requestName}"\n`);
    script2.write(`    git merge --squash ${item.branchName}\n`);
    script2.write(
      `    git commit -m "${item.requestName}" --author="${item.author}"\n`
    );
    script2.write(`else\n`);
    script2.write(`    echo "Already merged ${item.requestName}"\n`);
    script2.write("fi\n\n");

    script2.write(`if [[ $(git log | grep -c "${requestName}") = 0 ]]; then\n`);
    script2.write(`    exit 1\n`);
    script2.write("fi\n\n");

    script1.write(`git merge -m "${item.requestName}" ${item.branchName}\n`);
  });

  console.log("Wrote simple merge script to:", script1.path);
  console.log("Wrote squash merge script to:", script2.path);

  script1.end();
  script2.end();
}

main({ maxPages: 5 });
