# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "2.02 Lang Detect"
OutFile "202_lang_detec.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"

Var LANG

;;
; Main component
;;
Section

		ReadRegStr $LANG HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" Language

		MessageBox MB_YESNO|MB_ICONEXCLAMATION "Your language is $LANG"
	
	;WriteRegStr HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" "Language" "english"


SectionEnd



