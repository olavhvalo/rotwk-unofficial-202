# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "2.02 German Language Pack"
OutFile "202_v8_German_language.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"

Var LANG

;;
; Main component
;;
Section
	DetailPrint "Determining language..."
	
	ReadRegStr $LANG HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" Language
	DetailPrint "Your language is: $LANG"

	;Check if language is what we expect
	StrCmp $LANG "German" 0 noteq
		DetailPrint "Your game language is German."
		goto end
		
	noteq:
		DetailPrint "Your game language is not German."
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "Your game does not seem to be installed in German. Are you sure you want to install the German language pack anyway?"  IDYES true IDNO false
		true:
			DetailPrint "Installing anyway..."
			Goto next
		false:
			DetailPrint "Aborting..."
			Abort
		next:
		
	end:

	DetailPrint "Finding 2.02 v8.0.1..."
	;Check if v8 is installed
	${If} ${FileExists} "$INSTDIR\###########202_v8.0.1.big" 
	${OrIf} ${FileExists} "$INSTDIR\###########202_v8.0.1.disabled"
		DetailPrint "Good, 2.02 v8.0.1 is present."
	${Else}
		DetailPrint "2.02 v8.0.1 not found!"
		MessageBox MB_OK|MB_ICONSTOP "2.02 v8.0.1 not found! Please install 2.02 v8.0.1 first before using this language pack."
		Abort
	${EndIf}
	
	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	File /r "V8-German/*"
	Delete "$INSTDIR\lang\germanstrings202_v7.0.0.big"
	Delete "$INSTDIR\launcher_releases\2.01 German.ini"
	Delete "$INSTDIR\launcher_releases\2.02 v7.0.0 German.ini"

SectionEnd



